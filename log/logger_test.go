package log

import (
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/qiankaihua/go-http-server-common/mock"
)

func TestLogger(t *testing.T) {
	a := assert.New(t)

	os.RemoveAll("output/log-test")
	err := InitLogger(LoggerConf{
		ServerName:      "go-http-server-common",
		FilterFunc:      nil,
		LogMinLevel:     LogLevelDebug,
		LogDirName:      "output/log-test",
		NoticeFileLevel: DefaultNoticeFileLevel,
		WfFileLevel:     DefaultNoticeWfFileLevel,
	})
	os.RemoveAll("output/log-test")
	a.Nil(err)
	a.NotNil(DefaultLogger)

	logger := DefaultLogger
	engine := &mock.LogEngine{
		Last: map[mock.LogLevel]string{},
	}
	logger.LogEngine[wfEngineName] = engine
	logger.LogEngine[notiiceEngineName] = engine

	Debug("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelDebug, "", "123", 1231, "fds"))
	Info("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelInfo, "", "123", 1231, "fds"))
	Warn("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelWarning, "", "123", 1231, "fds"))
	Error("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelError, "", "123", 1231, "fds"))
	Fatal("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelFatal, "", "123", 1231, "fds"))
	Panic("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelPanic, "", "123", 1231, "fds"))
	DebugF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelDebug, "%s, %d, %v", "123", 1231, "fds"))
	InfoF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelInfo, "%s, %d, %v", "123", 1231, "fds"))
	WarnF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelWarning, "%s, %d, %v", "123", 1231, "fds"))
	ErrorF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelError, "%s, %d, %v", "123", 1231, "fds"))
	FatalF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelFatal, "%s, %d, %v", "123", 1231, "fds"))
	PanicF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelPanic, "%s, %d, %v", "123", 1231, "fds"))

	logger.LogEngine = map[string]logEngine{}
	logger.Debug("123", 1231, "fds")
	logger.Info("123", 1231, "fds")
	logger.Warn("123", 1231, "fds")
	logger.Error("123", 1231, "fds")
	logger.Fatal("123", 1231, "fds")
	logger.Panic("123", 1231, "fds")
	logger.DebugF("%s, %d, %v", "123", 1231, "fds")
	logger.InfoF("%s, %d, %v", "123", 1231, "fds")
	logger.WarnF("%s, %d, %v", "123", 1231, "fds")
	logger.ErrorF("%s, %d, %v", "123", 1231, "fds")
	logger.FatalF("%s, %d, %v", "123", 1231, "fds")
	logger.PanicF("%s, %d, %v", "123", 1231, "fds")
}

func TestCustomLogger(t *testing.T) {
	a := assert.New(t)

	os.RemoveAll("output/log-test")
	logger, err := NewLogger(LoggerConf{
		ServerName:      "go-http-server-common",
		FilterFunc:      nil,
		LogMinLevel:     LogLevelDebug,
		LogDirName:      "output/log-test",
		NoticeFileLevel: DefaultNoticeFileLevel,
		WfFileLevel:     DefaultNoticeWfFileLevel,
	})
	os.RemoveAll("output/log-test")
	a.Nil(err)
	a.NotNil(logger)
}

func TestWriteLog(t *testing.T) {
	a := assert.New(t)

	logger, err := NewLogger(LoggerConf{
		ServerName:      "go-http-server-common",
		FilterFunc:      nil,
		LogMinLevel:     LogLevelDebug,
		LogDirName:      "output/log-test",
		NoticeFileLevel: DefaultNoticeFileLevel,
		WfFileLevel:     DefaultNoticeWfFileLevel,
	})
	a.Nil(err)
	a.NotNil(logger)
	engine := &mock.LogEngine{
		Last: map[mock.LogLevel]string{},
	}
	logger.LogEngine[wfEngineName] = engine
	logger.LogEngine[notiiceEngineName] = engine

	logger.Debug("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelDebug, "", "123", 1231, "fds"))
	logger.Info("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelInfo, "", "123", 1231, "fds"))
	logger.Warn("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelWarning, "", "123", 1231, "fds"))
	logger.Error("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelError, "", "123", 1231, "fds"))
	logger.Fatal("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelFatal, "", "123", 1231, "fds"))
	logger.Panic("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelPanic, "", "123", 1231, "fds"))
	logger.DebugF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelDebug, "%s, %d, %v", "123", 1231, "fds"))
	logger.InfoF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelInfo, "%s, %d, %v", "123", 1231, "fds"))
	logger.WarnF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelWarning, "%s, %d, %v", "123", 1231, "fds"))
	logger.ErrorF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelError, "%s, %d, %v", "123", 1231, "fds"))
	logger.FatalF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelFatal, "%s, %d, %v", "123", 1231, "fds"))
	logger.PanicF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelPanic, "%s, %d, %v", "123", 1231, "fds"))
}

func TestLoggerLevel(t *testing.T) {
	a := assert.New(t)

	type Case struct {
		Expect logrus.Level
		Input  LogLevel
	}
	cases := []Case{
		{
			Expect: logrus.DebugLevel,
			Input:  LogLevelDebug,
		},
		{
			Expect: logrus.InfoLevel,
			Input:  LogLevelInfo,
		},
		{
			Expect: logrus.WarnLevel,
			Input:  LogLevelWarning,
		},
		{
			Expect: logrus.ErrorLevel,
			Input:  LogLevelError,
		},
		{
			Expect: logrus.FatalLevel,
			Input:  LogLevelFatal,
		},
		{
			Expect: logrus.PanicLevel,
			Input:  LogLevelPanic,
		},
		{
			Expect: logrus.WarnLevel,
			Input:  "default",
		},
	}

	for _, c := range cases {
		a.Equal(c.Expect, getLogLevel(c.Input))
	}
}
