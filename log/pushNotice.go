package log

import (
	"fmt"
	"strings"
)

type entry struct {
	Key   string
	Value string
}

type NoticeEntry struct {
	entrys []entry
}

// MARK: 并发不安全
func NewNoticeEntry() *NoticeEntry {
	return &NoticeEntry{
		entrys: []entry{},
	}
}

func (ne *NoticeEntry) PushNotice(key string, value interface{}) {
	ne.entrys = append(ne.entrys, entry{
		Key:   key,
		Value: fmt.Sprintf("%v", value),
	})
}
func (ne *NoticeEntry) PushNoticeF(key, format string, value ...interface{}) {
	ne.entrys = append(ne.entrys, entry{
		Key:   key,
		Value: fmt.Sprintf(format, value...),
	})
}
func (ne *NoticeEntry) String() string {
	builder := strings.Builder{}
	for _, entry := range ne.entrys {
		builder.WriteString(fmt.Sprintf("[%s=%s]", entry.Key, entry.Value))
	}
	return builder.String()
}
