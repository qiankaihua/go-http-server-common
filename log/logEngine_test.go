package log

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogEngine(t *testing.T) {
	a := assert.New(t)
	log := logEngineImpl{}
	log.SetOut(os.Stdout)
	a.Equal(os.Stdout, log.Out)
}
