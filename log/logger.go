package log

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/qiankaihua/go-http-server-common/common"
	"gopkg.in/natefinch/lumberjack.v2"
)

type LogLevel string

var (
	LogLevelDebug   LogLevel = "Debug"
	LogLevelInfo    LogLevel = "Info"
	LogLevelWarning LogLevel = "Warning"
	LogLevelError   LogLevel = "Error"
	LogLevelPanic   LogLevel = "Panic"
	LogLevelFatal   LogLevel = "Fatal"
)

type Logger struct {
	LogEngine map[string]logEngine
}

type LoggerConf struct {
	ServerName          string
	FilterFunc          filterFunc
	LogMinLevel         LogLevel
	LogDirName          string
	NoticeFileLevel     []LogLevel
	WfFileLevel         []LogLevel
	SingleFileMaxSizeMB int
	MaxFileNum          int
}

var (
	DefaultLogger            *Logger = &Logger{}
	DefaultNoticeFileLevel           = []LogLevel{LogLevelDebug, LogLevelInfo}
	DefaultNoticeWfFileLevel         = []LogLevel{LogLevelWarning, LogLevelError, LogLevelPanic, LogLevelFatal}
	notiiceEngineName                = "notice"
	wfEngineName                     = "wf"
)

func NewLogger(conf LoggerConf) (*Logger, error) {
	log := &Logger{}
	err := initLogger(log, conf)
	if err != nil {
		return nil, fmt.Errorf("new Logger failed, err=%v", err)
	}
	return log, nil
}

func InitLogger(conf LoggerConf) error {
	return initLogger(DefaultLogger, conf)
}

func initLogger(log *Logger, conf LoggerConf) error {
	// fix logger conf
	if conf.MaxFileNum <= 0 {
		conf.MaxFileNum = 3
	}
	if conf.SingleFileMaxSizeMB <= 0 {
		conf.SingleFileMaxSizeMB = 128
	}

	exist, err := common.PathExists(conf.LogDirName)
	if err != nil {
		return fmt.Errorf("check config dir[%s] failed: %v", conf.LogDirName, err)
	}
	if !exist {
		errMkdir := os.MkdirAll(conf.LogDirName, os.ModePerm)
		if errMkdir != nil {
			return fmt.Errorf("create config dir[%s] failed: %v", conf.LogDirName, errMkdir)
		}
	}

	log.LogEngine = make(map[string]logEngine)
	log.LogEngine[notiiceEngineName] = &logrus.Logger{
		Out: &lumberjack.Logger{
			Filename:   conf.LogDirName + "/" + conf.ServerName + ".log",
			MaxSize:    conf.SingleFileMaxSizeMB, // megabytes
			MaxBackups: conf.MaxFileNum,
			MaxAge:     30,    //days
			Compress:   false, // disabled by default
		},
		Level:     getLogLevel(conf.LogMinLevel),
		Formatter: NewTextFormat(),
		Hooks:     make(logrus.LevelHooks),
	}
	log.LogEngine[notiiceEngineName].AddHook(NewLineHook(conf.ServerName, conf.FilterFunc))

	log.LogEngine[wfEngineName] = &logrus.Logger{
		Out: &lumberjack.Logger{
			Filename:   conf.LogDirName + "/" + conf.ServerName + ".log.wf",
			MaxSize:    conf.SingleFileMaxSizeMB, // megabytes
			MaxBackups: conf.MaxFileNum,
			MaxAge:     30,    //days
			Compress:   false, // disabled by default
		},
		Level:     getLogLevel(conf.LogMinLevel),
		Formatter: NewTextFormat(),
		Hooks:     make(logrus.LevelHooks),
	}
	log.LogEngine[wfEngineName].AddHook(NewLineHook(conf.ServerName, conf.FilterFunc))

	return nil
}

func Debug(params ...interface{}) {
	DefaultLogger.LogEngine[notiiceEngineName].Debug(params...)
}
func DebugF(format string, params ...interface{}) {
	DefaultLogger.LogEngine[notiiceEngineName].Debugf(format, params...)
}
func Info(params ...interface{}) {
	DefaultLogger.LogEngine[notiiceEngineName].Info(params...)
}
func InfoF(format string, params ...interface{}) {
	DefaultLogger.LogEngine[notiiceEngineName].Infof(format, params...)
}
func Warn(params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Warn(params...)
}
func WarnF(format string, params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Warnf(format, params...)
}
func Error(params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Error(params...)
}
func ErrorF(format string, params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Errorf(format, params...)
}
func Fatal(params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Fatal(params...)
}
func FatalF(format string, params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Fatalf(format, params...)
}
func Panic(params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Panic(params...)
}
func PanicF(format string, params ...interface{}) {
	DefaultLogger.LogEngine[wfEngineName].Panicf(format, params...)
}

func (l *Logger) Debug(params ...interface{}) {
	if logger, ok := l.LogEngine[notiiceEngineName]; ok {
		logger.Debug(params...)
		return
	}
	fmt.Println("[DEBUG]", params)
}
func (l *Logger) DebugF(format string, params ...interface{}) {
	if logger, ok := l.LogEngine[notiiceEngineName]; ok {
		logger.Debugf(format, params...)
		return
	}
	fmt.Printf("[DEBUG]"+format+"\n", params...)
}
func (l *Logger) Info(params ...interface{}) {
	if logger, ok := l.LogEngine[notiiceEngineName]; ok {
		logger.Info(params...)
		return
	}
	fmt.Println("[INFO]", params)
}
func (l *Logger) InfoF(format string, params ...interface{}) {
	if logger, ok := l.LogEngine[notiiceEngineName]; ok {
		logger.Infof(format, params...)
		return
	}
	fmt.Printf("[INFO]"+format+"\n", params...)
}
func (l *Logger) Warn(params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Warn(params...)
		return
	}
	fmt.Println("[WARN]", params)
}
func (l *Logger) WarnF(format string, params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Warnf(format, params...)
		return
	}
	fmt.Printf("[WARN]"+format+"\n", params...)
}
func (l *Logger) Error(params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Error(params...)
		return
	}
	fmt.Println("[ERROR]", params)
}
func (l *Logger) ErrorF(format string, params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Errorf(format, params...)
		return
	}
	fmt.Printf("[ERROR]"+format+"\n", params...)
}
func (l *Logger) Fatal(params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Fatal(params...)
		return
	}
	fmt.Println("[FATAL]", params)
}
func (l *Logger) FatalF(format string, params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Fatalf(format, params...)
		return
	}
	fmt.Printf("[FATAL]"+format+"\n", params...)
}
func (l *Logger) Panic(params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Panic(params...)
		return
	}
	fmt.Println("[PANIC]", params)
}
func (l *Logger) PanicF(format string, params ...interface{}) {
	if logger, ok := l.LogEngine[wfEngineName]; ok {
		logger.Panicf(format, params...)
		return
	}
	fmt.Printf("[PANIC]"+format+"\n", params...)
}

func getLogLevel(logLevelString LogLevel) logrus.Level {
	switch logLevelString {
	case LogLevelDebug:
		return logrus.DebugLevel
	case LogLevelInfo:
		return logrus.InfoLevel
	case LogLevelWarning:
		return logrus.WarnLevel
	case LogLevelError:
		return logrus.ErrorLevel
	case LogLevelPanic:
		return logrus.PanicLevel
	case LogLevelFatal:
		return logrus.FatalLevel
	default:
		return logrus.WarnLevel
	}
}
