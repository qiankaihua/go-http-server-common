package log

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPushNotice(t *testing.T) {
	a := assert.New(t)
	e := NewNoticeEntry()
	e.PushNotice("abc", 1234)
	e.PushNoticeF("test", "%d%s", 1234, "fsdfd")
	a.Equal("[abc=1234][test=1234fsdfd]", e.String())
}
