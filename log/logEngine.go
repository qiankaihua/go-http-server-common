package log

import (
	"io"

	"github.com/sirupsen/logrus"
)

type logEngine interface {
	Debugf(format string, args ...interface{})
	Debug(args ...interface{})
	Infof(format string, args ...interface{})
	Info(args ...interface{})
	Errorf(format string, args ...interface{})
	Error(args ...interface{})
	Warnf(format string, args ...interface{})
	Warn(args ...interface{})
	Fatalf(format string, args ...interface{})
	Fatal(args ...interface{})
	Panicf(format string, args ...interface{})
	Panic(args ...interface{})
	AddHook(hook logrus.Hook)
}

type logEngineImpl struct {
	logrus.Logger
}

func (log *logEngineImpl) SetOut(out io.Writer) {
	log.Out = out
}
