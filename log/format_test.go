package log

import (
	"bytes"
	"fmt"
	"runtime"
	"strings"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func formatData(level, message string, time time.Time, data map[string]interface{}) []byte {
	builder := strings.Builder{}
	builder.WriteString(fmt.Sprintf("[%s]", level))
	builder.WriteString(fmt.Sprintf("[%s]", time.Format(timeFormat)))
	for k, v := range data {
		builder.WriteString(fmt.Sprintf(dataFormat, k, v))
	}
	builder.WriteString(message)
	builder.WriteString("\n")
	return []byte(builder.String())
}

func TestFormat(t *testing.T) {
	a := assert.New(t)
	format := NewTextFormat()
	a.NotNil(format)

	entry := &logrus.Entry{
		Logger: &logrus.Logger{},
		Data: map[string]interface{}{
			"test": 123,
		},
		Time:    time.Now(),
		Level:   0,
		Caller:  &runtime.Frame{},
		Message: "",
		Buffer:  &bytes.Buffer{},
		Context: nil,
	}
	b, e := format.Format(entry)
	a.Nil(e)
	a.Equal(string(formatData(strings.ToUpper(entry.Level.String()), entry.Message, entry.Time, entry.Data)), string(b))
}
