package log

import (
	"fmt"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
)

type filterFunc func(string) bool

type Hook struct {
	Field      string
	Skip       int
	levels     []logrus.Level
	filterFunc filterFunc
	Formatter  func(file, function string, line int) string
	ServerName string
}

func (hook *Hook) Levels() []logrus.Level {
	return hook.levels
}

func (hook *Hook) Fire(entry *logrus.Entry) error {
	entry.Data[hook.Field] = hook.Formatter(hook.findCaller(hook.Skip))
	return nil
}

func NewLineHook(serverName string, customFileFilter filterFunc, levels ...logrus.Level) *Hook {
	hook := Hook{
		Field:  "source",
		Skip:   5,
		levels: levels,
		Formatter: func(file, function string, line int) string {
			return fmt.Sprintf("%s:%d", file, line)
		},
		filterFunc: customFileFilter,
		ServerName: serverName,
	}
	if len(hook.levels) == 0 {
		hook.levels = logrus.AllLevels
	}

	return &hook
}

func (hook *Hook) findCaller(skip int) (string, string, int) {
	var (
		pc       uintptr
		file     string
		function string
		line     int
	)
	for i := 0; i < 10; i++ {
		pc, file, line = hook.getCaller(skip + i)

		if strings.HasPrefix(file, "logrus") {
			continue
		}

		if strings.HasPrefix(file, "log/logger") {
			continue
		}
		if strings.HasPrefix(file, "http/context") {
			continue
		}

		if hook.filterFunc != nil && hook.filterFunc(file) {
			continue
		}
		break
	}
	if pc != 0 {
		frames := runtime.CallersFrames([]uintptr{pc})
		frame, _ := frames.Next()
		function = frame.Function
	}

	return file, function, line
}

func (hook *Hook) getCaller(skip int) (uintptr, string, int) {
	pc, file, line, ok := runtime.Caller(skip)
	if !ok {
		return 0, "", 0
	}
	n := 0
	t := strings.Split(file, hook.ServerName)
	if len(t) > 1 {
		return pc, t[len(t)-1], line
	}
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			n += 1
			if n >= 2 {
				file = file[i+1:]
				break
			}
		}
	}

	return pc, file, line
}
