package log

import (
	"bytes"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

const serverName = "go-http-server-common"
const runtimeName = "runtime"

func TestHook(t *testing.T) {
	a := assert.New(t)

	hook := NewLineHook(serverName, nil)
	a.Nil(hook.filterFunc)
	a.Equal(hook.Levels(), logrus.AllLevels)

	data := map[string]interface{}{
		"test": "test",
		"aaaa": 12345,
	}
	entry := &logrus.Entry{
		Logger:  &logrus.Logger{},
		Data:    data,
		Time:    time.Time{},
		Level:   0,
		Message: "",
		Buffer:  &bytes.Buffer{},
		Context: nil,
	}

	hook.Fire(entry)
	_, ok := entry.Data["source"]
	a.True(ok)
}

func TestFilterHook(t *testing.T) {
	a := assert.New(t)

	hook := NewLineHook(serverName, func(s string) bool {
		return true
	})

	data := map[string]interface{}{
		"test": "test",
		"aaaa": 12345,
	}
	entry := &logrus.Entry{
		Logger:  &logrus.Logger{},
		Data:    data,
		Time:    time.Time{},
		Level:   0,
		Message: "",
		Buffer:  &bytes.Buffer{},
		Context: nil,
	}

	hook.Fire(entry)
	v, ok := entry.Data["source"]
	a.Equal(":0", v)
	a.True(ok)
}

func TestPart(t *testing.T) {
	a := assert.New(t)

	hook := NewLineHook(runtimeName, nil)

	data := map[string]interface{}{
		"test": "test",
		"aaaa": 12345,
	}
	entry := &logrus.Entry{
		Logger:  &logrus.Logger{},
		Data:    data,
		Time:    time.Time{},
		Level:   0,
		Message: "",
		Buffer:  &bytes.Buffer{},
		Context: nil,
	}

	hook.Fire(entry)
	v, ok := entry.Data["source"]

	a.NotNil(v)
	a.True(ok)
}
