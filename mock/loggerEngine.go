package mock

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

type LogLevel string

var (
	LogLevelDebug   LogLevel = "Debug"
	LogLevelInfo    LogLevel = "Info"
	LogLevelWarning LogLevel = "Warning"
	LogLevelError   LogLevel = "Error"
	LogLevelPanic   LogLevel = "Panic"
	LogLevelFatal   LogLevel = "Fatal"
)

type LogEngine struct {
	Last map[LogLevel]string
}

func (l *LogEngine) Debug(args ...interface{}) {
	l.Last[LogLevelDebug] = fmt.Sprint(args...)
}

func (l *LogEngine) Debugf(format string, args ...interface{}) {
	l.Last[LogLevelDebug] = fmt.Sprintf(format, args...)
}

func (l *LogEngine) Info(args ...interface{}) {
	l.Last[LogLevelInfo] = fmt.Sprint(args...)
}

func (l *LogEngine) Infof(format string, args ...interface{}) {
	l.Last[LogLevelInfo] = fmt.Sprintf(format, args...)
}

func (l *LogEngine) Error(args ...interface{}) {
	l.Last[LogLevelError] = fmt.Sprint(args...)
}

func (l *LogEngine) Errorf(format string, args ...interface{}) {
	l.Last[LogLevelError] = fmt.Sprintf(format, args...)
}

func (l *LogEngine) Warn(args ...interface{}) {
	l.Last[LogLevelWarning] = fmt.Sprint(args...)
}

func (l *LogEngine) Warnf(format string, args ...interface{}) {
	l.Last[LogLevelWarning] = fmt.Sprintf(format, args...)
}

func (l *LogEngine) Fatal(args ...interface{}) {
	l.Last[LogLevelFatal] = fmt.Sprint(args...)
}

func (l *LogEngine) Fatalf(format string, args ...interface{}) {
	l.Last[LogLevelFatal] = fmt.Sprintf(format, args...)
}

func (l *LogEngine) Panic(args ...interface{}) {
	l.Last[LogLevelPanic] = fmt.Sprint(args...)
}

func (l *LogEngine) Panicf(format string, args ...interface{}) {
	l.Last[LogLevelPanic] = fmt.Sprintf(format, args...)
}

func (l *LogEngine) Check(level LogLevel, format string, args ...interface{}) bool {
	if v, ok := l.Last[level]; ok {
		if format == "" {
			return v == fmt.Sprint(args...)
		} else {
			return v == fmt.Sprintf(format, args...)
		}
	}
	return false
}

func (l *LogEngine) AddHook(hook logrus.Hook) {
	return
}
