package main

import (
	"fmt"
	"regexp"
	"strings"
)

// 检查 struct 各个字段的类型，基础类型是否是 bool/int32/int64/string/float/double 复合类型包括 map<a, b> 和 list<a> 自定义类型 struct 名字是否都存在
// struct命名规则 /^[a-zA-Z][\w_]+[a-zA-Z]$/ 首字母大写之后check 重复（由于可见性要求）
// 检查 interface 出入参是否定义，url路径+请求方法是否重复, 请求方法是否属于 GET/POST/PUT/DELETE / 全转大写比较
const (
	interfaceNameReg = `^[A-Z][a-zA-Z0-9_]*$`
	structNameReg    = `^[A-Z][a-zA-Z0-9_]*$`
	fieldNameReg     = `^[A-Za-z][a-zA-Z0-9_]*$`
)

var (
	baseTypeMap = map[FieldType]bool{
		FieldTypeBool:   true,
		FieldTypeInt32:  true,
		FieldTypeInt64:  true,
		FieldTypeUint32: true,
		FieldTypeUint64: true,
		FieldTypeString: true,
		FieldTypeFloat:  true,
		FieldTypeDouble: true,
	}
	mapTypeMap = map[FieldType]string{
		FieldTypeFile: "*multipart.FileHeader",
	}
	comflexTypeMap = map[FieldType]string{
		FieldTypeList: `^list *< *([a-zA-Z][a-zA-Z0-9_<]* *>?) *>$`,
		FieldTypeMap:  `^map *< *([a-zA-Z][a-zA-Z0-9_<]* *>?) *, *([a-zA-Z][a-zA-Z0-9_<]* *>?) *>$`,
	}
	comflexTypeFormatMap = map[FieldType]string{
		FieldTypeList: "[]%s",
		FieldTypeMap:  "map[%s]%s",
	}
)

func CheckInterface(data map[string]*InterfaceDefine, structName map[string]*StructDefine) error {
	var (
		err    error
		dupMap = make(map[string]bool)
	)
	for _, define := range data {
		err = checkInterfaceName(define.Name)
		if err != nil {
			return fmt.Errorf("check interface[%s] name error, name=%s err=%v", define.Name, define.Name, err)
		}
		err = checkInterfaceMethod(define.HttpMethod)
		if err != nil {
			return fmt.Errorf("check interface[%s] http_method error, http_method=%s err=%v", define.Name, define.HttpMethod, err)
		}
		err = checkInterfaceUrlDuplicated(define.Url, define.HttpMethod, dupMap)
		if err != nil {
			return fmt.Errorf("check interface[%s] url error, url=%s err=%v", define.Name, define.Url, err)
		}
		err = checkInterfaceParams(define, structName)
		if err != nil {
			return fmt.Errorf("check interface[%s] params error, inputParams=%s, outputParams=%s, err=%v", define.Name, define.InputStruct, define.OutputStruct, err)
		}
	}
	return nil
}

func CheckStruct(data map[string]*StructDefine) error {
	var (
		err    error
		dupMap = make(map[string]bool)
	)
	for _, single := range data {
		err = checkStructName(single.Name)
		if err != nil {
			return fmt.Errorf("check struct[%s] name error, name=%s err=%v", single.Name, single.Name, err)
		}
		err = checkStructDuplicated(single.Name, dupMap)
		if err != nil {
			return fmt.Errorf("check struct[%s] name error, name=%s err=%v", single.Name, single.Name, err)
		}
		err = checkStructField(single, data)
		if err != nil {
			return fmt.Errorf("check struct[%s] field error, err=%v", single.Name, err)
		}
	}
	return nil
}

func checkInterfaceName(name string) error {
	ok, err := regexp.MatchString(interfaceNameReg, name)
	if err != nil {
		return err
	}
	if !ok {
		return fmt.Errorf("name[%s] not match CamelType", name)
	}
	return nil
}

func checkInterfaceParams(data *InterfaceDefine, structName map[string]*StructDefine) error {
	if _, ok := structName[data.InputStruct]; !ok {
		return fmt.Errorf("interface[%s] input[%s] not exist", data.Name, data.InputStruct)
	}
	if _, ok := structName[data.OutputStruct]; !ok {
		return fmt.Errorf("interface[%s] output[%s] not exist", data.Name, data.OutputStruct)
	}
	return nil
}

func checkInterfaceMethod(method HTTPMethod) error {
	if method == HTTPMethodGet ||
		method == HTTPMethodPost ||
		method == HTTPMethodPut ||
		method == HTTPMethodDelete {
		return nil
	}
	return fmt.Errorf("method[%s] not valid", method)
}

func checkInterfaceUrlDuplicated(url string, method HTTPMethod, dupMap map[string]bool) error {
	uniKey := url + string(method)
	if _, ok := dupMap[uniKey]; ok {
		return fmt.Errorf("url[%s] method[%s] is duplicate", url, method)
	}
	dupMap[uniKey] = true
	return nil
}

func checkStructName(name string) error {
	ok, err := regexp.MatchString(structNameReg, name)
	if err != nil {
		return err
	}
	if !ok {
		return fmt.Errorf("name[%s] not match CamelType", name)
	}
	return nil
}

func checkStructDuplicated(name string, dupMap map[string]bool) error {
	if _, ok := dupMap[name]; ok {
		return fmt.Errorf("struct[%s] is duplicate", name)
	}
	dupMap[name] = true
	return nil
}

func checkStructField(single *StructDefine, data map[string]*StructDefine) error {
	dupMap := make(map[string]bool)
	for _, f := range single.Field {
		err := checkFieldName(f.Name)
		if err != nil {
			return fmt.Errorf("struct[%s] check filed error, err=%v", single.Name, err)
		}
		if _, ok := dupMap[strings.ToLower(f.Name)]; ok {
			return fmt.Errorf("struct[%s], filed name is dup, dup name=%s", single.Name, f.Name)
		}
		dupMap[strings.ToLower(f.Name)] = true
		f.DependName = map[string]bool{}
		err = checkType(f, f.Type, data)
		if err != nil {
			return fmt.Errorf("struct[%s] check filed error, err=%v", single.Name, err)
		}
	}
	return nil
}

func checkFieldName(name string) error {
	ok, err := regexp.MatchString(fieldNameReg, name)
	if err != nil {
		return err
	}
	if !ok {
		return fmt.Errorf("name[%s] not match CamelType", name)
	}
	return nil
}

func checkType(single *Field, checkTypeName FieldType, data map[string]*StructDefine) error {
	// 基础类型
	if _, ok := baseTypeMap[checkTypeName]; ok {
		single.DependName[string(checkTypeName)] = true
		single.TrueType = checkTypeName
		single.TypeName = string(checkTypeName)
		return nil
	}
	if typeName, ok := mapTypeMap[checkTypeName]; ok {
		single.DependName[string(checkTypeName)] = true
		single.TrueType = checkTypeName
		single.TypeName = typeName
		return nil
	}
	// 复合类型
	isComflex := false
	for typ, regStr := range comflexTypeMap {
		reg, err := regexp.Compile(regStr)
		if err != nil {
			return fmt.Errorf("field[%s] check err, exp[%s] compile err=%v", single.Name, regStr, err)
		}
		resMap := reg.FindAllString(string(checkTypeName), 10)
		if len(resMap) != 1 {
			continue
		}
		isComflex = true
		resSlice := reg.FindAllStringSubmatch(string(checkTypeName), 10)
		formatValue := make([]interface{}, 0)
		for i := 1; i < len(resSlice[0]); i++ {
			err := checkType(single, FieldType(resSlice[0][i]), data)
			if err != nil {
				return err
			}
			single.DependName[resSlice[0][i]] = true
			formatValue = append(formatValue, single.TypeName)
		}
		single.TrueType = typ
		single.TypeName = fmt.Sprintf(comflexTypeFormatMap[typ], formatValue...)
		break
	}
	if isComflex {
		return nil
	}
	// 自定义类型
	if _, ok := data[string(checkTypeName)]; ok {
		single.DependName[string(checkTypeName)] = true
		single.TrueType = FieldTypeStruct
		single.TypeName = "*" + string(checkTypeName)
		return nil
	}
	return fmt.Errorf("type[%s] is not valid", checkTypeName)
}
