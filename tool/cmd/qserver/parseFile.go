package main

import (
	"fmt"
	"sort"
	"strings"

	goConf "github.com/parkingwang/go-conf"
	"gitlab.com/qiankaihua/go-http-server-common/common"
)

type FieldType string
type HTTPMethod string

const (
	FieldTypeInt32  FieldType = "int32"
	FieldTypeInt64  FieldType = "int64"
	FieldTypeUint32 FieldType = "uint32"
	FieldTypeUint64 FieldType = "uint64"
	FieldTypeFloat  FieldType = "float"
	FieldTypeDouble FieldType = "double"
	FieldTypeString FieldType = "string"
	FieldTypeMap    FieldType = "map"
	FieldTypeList   FieldType = "list"
	FieldTypeBool   FieldType = "bool"
	FieldTypeStruct FieldType = "struct"
	FieldTypeFile   FieldType = "file"

	HTTPMethodGet    HTTPMethod = "GET"
	HTTPMethodPost   HTTPMethod = "POST"
	HTTPMethodPut    HTTPMethod = "PUT"
	HTTPMethodDelete HTTPMethod = "DELETE"
)

type InterfaceDefine struct {
	InputStruct  string
	OutputStruct string
	Url          string
	HttpMethod   HTTPMethod
	Name         string
}

type Field struct {
	Name       string
	Type       FieldType
	TypeName   string
	DependName map[string]bool
	TrueType   FieldType
	Optional   bool
}

type StructDefine struct {
	Name  string
	Field []*Field
}

const (
	interfaceName  = "interface"
	inputName      = "input"
	outputName     = "output"
	urlName        = "url"
	httpMethodName = "method"
	structName     = "struct"
	moduleConfName = "module"
)

var (
	moduleName = ""
	apiPrefix  = ""
)

func Parse(confName string) (map[string]*InterfaceDefine, map[string]*StructDefine, error) {
	if exist, err := common.FileExists(confName); err != nil || !exist {
		return nil, nil, fmt.Errorf("file not exist")
	}
	conf, err := goConf.LoadConfig(confName)
	if err != nil {
		return nil, nil, err
	}
	confMp := conf.RefMap()

	resInterface := make(map[string]*InterfaceDefine)
	if interfaceMp, ok := confMp[interfaceName]; ok {
		resInterface, err = handleInterface(interfaceMp.(map[string]interface{}))
		if err != nil {
			return nil, nil, err
		}
	}

	resStruct := make(map[string]*StructDefine)
	if structMp, ok := confMp[structName]; ok {
		resStruct, err = handleStruct(structMp.(map[string]interface{}))
		if err != nil {
			return nil, nil, err
		}
	}

	if mp, ok := confMp[moduleConfName]; ok {
		m, ok := mp.(map[string]interface{})
		if ok {
			for k, v := range m {
				if strings.ToLower(k) == "name" {
					s, ok := v.(string)
					if ok {
						moduleName = s
					}
				}
				if strings.ToLower(k) == "apiprefix" {
					s, ok := v.(string)
					if ok {
						apiPrefix = s
					}
				}
			}
		}
	}

	return resInterface, resStruct, nil
}

func handleInterface(mp map[string]interface{}) (map[string]*InterfaceDefine, error) {
	res := make(map[string]*InterfaceDefine)
	for k, v := range mp {
		if _, ok := res[k]; ok {
			return nil, fmt.Errorf("duplicate interface name[%s]", k)
		}
		m, ok := v.(map[string]interface{})
		if !ok {
			return nil, fmt.Errorf("get single interface conf type error, value=%+v", v)
		}
		inputStruct, ok := m[inputName].(string)
		if !ok {
			return nil, fmt.Errorf("single interface conf not have input conf or type error, value=%+v", v)
		}
		outputStruct, ok := m[outputName].(string)
		if !ok {
			return nil, fmt.Errorf("single interface conf not have output conf or type error, value=%+v", v)
		}
		url, ok := m[urlName].(string)
		if !ok {
			return nil, fmt.Errorf("single interface conf not have url conf or type error, value=%+v", v)
		}
		method, ok := m[httpMethodName].(string)
		if !ok {
			return nil, fmt.Errorf("single interface conf not have method conf or type error, value=%+v", v)
		}
		if len(url) <= 0 || url[0] == '/' {
			return nil, fmt.Errorf("interface url can not be empty or start with '/', url=%+v", url)
		}
		res[k] = &InterfaceDefine{
			InputStruct:  inputStruct,
			OutputStruct: outputStruct,
			Url:          url,
			Name:         k,
			HttpMethod:   HTTPMethod(strings.ToUpper(method)),
		}
	}
	return res, nil
}

func handleStruct(mp map[string]interface{}) (map[string]*StructDefine, error) {
	res := make(map[string]*StructDefine)
	for k, v := range mp {
		if _, ok := res[k]; ok {
			return nil, fmt.Errorf("duplicate struct name[%s]", k)
		}
		m, ok := v.(map[string]interface{})
		if !ok {
			return nil, fmt.Errorf("get single interface conf type error, value=%+v", v)
		}
		fileds := make([]*Field, 0)

		for fk, fv := range m {
			vType, ok := fv.(string)
			if !ok {
				return nil, fmt.Errorf("struct[%s] conf fieldp[%s] type error, need string, get=%+v", k, fk, fv)
			}
			trueType := vType
			isOptional := false
			if vType != "" && vType[len(vType)-1] == '?' {
				trueType = string(vType[:len(vType)-1])
				isOptional = true
			}
			fileds = append(fileds, &Field{
				Name:       fk,
				Type:       FieldType(trueType),
				DependName: map[string]bool{},
				Optional:   isOptional,
			})
		}

		sort.Slice(fileds, func(i, j int) bool {
			return fileds[i].Name < fileds[j].Name
		})

		res[k] = &StructDefine{
			Name:  k,
			Field: fileds,
		}
	}
	return res, nil
}
