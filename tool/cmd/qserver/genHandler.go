package main

import (
	"fmt"
	"os/exec"
	"strings"
)

const (
	genHandlerDir     = "/handler"
	genHandlerPackage = "handler"
	genHandlerFile    = "%s.go"
)

var (
	handlerImportPath = "../handler"
)

func GenHandler(dir string, data map[string]*InterfaceDefine) error {
	if moduleName != "" {
		handlerImportPath = moduleName + genHandlerDir
	}
	if dir == "" {
		dir = "."
	}
	err := createDirWhenNotExist(dir + genHandlerDir)
	if err != nil {
		return fmt.Errorf("gen handler failed, create dir[%s/%s] failed, err=%s", dir, genHandlerDir, err)
	}
	for _, v := range data {
		ok, err := fileExists(dir + genHandlerDir + "/" + v.Name + ".go")
		if err != nil {
			return fmt.Errorf("check file[%s%s/%s] exist failed, err=%s", dir, genHandlerDir, v.Name, err)
		}
		if ok {
			continue
		}
		err = createDirAndFileWhenNotExist(dir+genHandlerDir, v.Name+".go")
		if err != nil {
			return fmt.Errorf("create file[%s%s/%s.go] failed, err=%s", dir, genHandlerDir, v.Name, err)
		}
		err = writeContentToFile(dir+genHandlerDir, v.Name+".go", genOneHandler(dir, v))
		if err != nil {
			return fmt.Errorf("write to file[%s%s/%s.go] failed, err=%s", dir, genHandlerDir, v.Name, err)
		}
	}
	cmd := exec.Command("gofmt", "-s", "-w", dir+genHandlerDir)
	if output, err := cmd.CombinedOutput(); err != nil {
		fmt.Printf("gen handler succ, but format dir[%s] failed, output=%s, err=%s\n", dir+genHandlerDir, string(output), err)
	}
	return nil
}

func genOneHandler(dir string, data *InterfaceDefine) string {
	builder := strings.Builder{}
	builder.WriteString(fmt.Sprintf("package %s\n", genHandlerPackage))
	builder.WriteString("import (\n")
	builder.WriteString("\t\"gitlab.com/qiankaihua/go-http-server-common/http\"\n")
	builder.WriteString(fmt.Sprintf("\t\"%s\"\n", modelImportPath))
	builder.WriteString(")\n\n")
	builder.WriteString(fmt.Sprintf("// @router %s [%s]\n", data.Url, data.HttpMethod))
	builder.WriteString(fmt.Sprintf("func %s(c *http.Context) {\n", data.Name))
	builder.WriteString("\tvar err error\n")
	builder.WriteString(fmt.Sprintf("\tvar req model.%s\n", data.InputStruct))
	builder.WriteString(fmt.Sprintf("\tresp := new(model.%s)\n\n", data.OutputStruct))
	builder.WriteString("\terr = c.Bind(&req)\n")
	builder.WriteString("\tif err != nil {\n")
	builder.WriteString("\t\tc.WarnF(\"bind param failed, err:%v\", err)\n")
	builder.WriteString("\t\t// TODO: write params error resp here\n")
	builder.WriteString("\t\tc.JSON(422, resp)\n")
	builder.WriteString("\t\treturn\n")
	builder.WriteString("\t}\n\n")
	builder.WriteString("\t// TODO: write your code here\n\n")
	builder.WriteString("\tc.JSON(200, resp)\n")
	builder.WriteString("}\n")
	return builder.String()
}
