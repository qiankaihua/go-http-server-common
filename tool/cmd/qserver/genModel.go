package main

import (
	"fmt"
	"os/exec"
	"sort"
	"strings"
)

const (
	genModelFilename = "model_gen.go"
	genModelDir      = "/model"
	genModelPackage  = "model"
)

var (
	modelImportPath = "../model"
	typeHeader      = map[FieldType]string{
		FieldTypeFile: "mime/multipart",
	}
)

func GenModel(dir string, data map[string]*StructDefine) error {
	if moduleName != "" {
		modelImportPath = moduleName + genModelDir
	}
	if dir == "" {
		dir = "."
	}
	content := genModelContent(data)
	err := createDirAndFileWhenNotExist(dir+genModelDir, genModelFilename)
	if err != nil {
		return fmt.Errorf("gen model failed, create file[%s/%s] failed, err=[%s]", dir+genModelDir, genModelFilename, err)
	}
	err = writeContentToFile(dir+genModelDir, genModelFilename, content)
	if err != nil {
		return fmt.Errorf("gen model failed, write file[%s/%s] failed, err=[%s]", dir+genModelDir, genModelFilename, err)
	}
	cmd := exec.Command("gofmt", "-w", fmt.Sprintf("%s/%s", dir+genModelDir, genModelFilename))
	if output, err := cmd.CombinedOutput(); err != nil {
		fmt.Printf("gen model succ, but format file[%s/%s] failed, output=%s, err=%s\n", dir+genModelDir, genModelFilename, string(output), err)
	}
	return nil
}

func genModelContent(data map[string]*StructDefine) string {
	builder := strings.Builder{}
	functionBuilder := strings.Builder{}
	structBuilder := strings.Builder{}
	builder.WriteString("// this file is generate by qserver tool, do not modify it.\n\n")
	builder.WriteString(fmt.Sprintf("package %s\n", genModelPackage))
	keys := make([]string, 0, len(data))
	for k := range data {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})
	importMap := make(map[string]bool)
	for _, key := range keys {
		structDefine, functionDefine := genOneModelContent(data[key], importMap)
		structBuilder.WriteString(structDefine + "\n")
		functionBuilder.WriteString(functionDefine + "\n")
	}
	builder.WriteString("import (\n")
	for k := range importMap {
		builder.WriteString("\"" + k + "\"\n")
	}
	builder.WriteByte(')')
	builder.WriteByte('\n')
	return builder.String() + structBuilder.String() + functionBuilder.String()
}

func genOneModelContent(data *StructDefine, importMap map[string]bool) (string, string) {
	builder := strings.Builder{}
	functionBuilder := strings.Builder{}
	builder.WriteString(fmt.Sprintf("type %s struct {\n", data.Name))
	for _, f := range data.Field {
		if importContent, ok := typeHeader[f.TrueType]; ok {
			importMap[importContent] = true
		}
		optionalPointer := ""
		if f.TrueType != FieldTypeStruct && f.TrueType != FieldTypeMap && f.TrueType != FieldTypeList && f.TrueType != FieldTypeFile && f.Optional {
			optionalPointer = "*"
		}
		builder.WriteString(fmt.Sprintf("\t%s %s%s %s\n", genFieldName(f.Name), optionalPointer, f.TypeName, genTag(f.Name, !f.Optional)))

		if f.Optional {
			functionBuilder.WriteString(genFieldGetterAndSetter(data.Name, f))
		}
	}
	builder.WriteString("}\n")
	return builder.String(), functionBuilder.String()
}

func genFieldName(name string) string {
	FieldName := name
	if name == "" {
		return ""
	}
	if name[0] >= 'a' && name[0] <= 'z' {
		FieldName = strings.ToUpper(name[0:1]) + name[1:]
	}
	return FieldName
}

func genTag(name string, required bool) string {
	r := ""
	omit := ",omitempty"
	if required {
		r = "binding:\"required\""
		omit = ""
	}
	return fmt.Sprintf("`form:\"%s%s\" json:\"%s%s\" query:\"%s%s\" %s`", name, omit, name, omit, name, omit, r)
}

func genFieldGetterAndSetter(structName string, field *Field) string {
	fieldName := genFieldName(field.Name)
	builder := strings.Builder{}
	typeName := field.TypeName
	getPoint := "&"

	if field.TrueType == FieldTypeStruct || field.TrueType == FieldTypeMap || field.TrueType == FieldTypeList {
		return genFieldGetterAndSetterWithPointerType(structName, field)
	}
	// setter
	builder.WriteString(fmt.Sprintf("func (c *%s) Set%s(value %s) {\n", structName, fieldName, typeName))
	builder.WriteString(fmt.Sprintf("\tc.%s = %svalue\n}\n", fieldName, getPoint))

	// getter
	defaultName := fmt.Sprintf("%s%sDefault", structName, fieldName)
	builder.WriteString(fmt.Sprintf("var %s %v\n", defaultName, field.TypeName))
	builder.WriteString(fmt.Sprintf("func (c *%s) Get%s() %s {\n", structName, fieldName, field.TypeName))
	builder.WriteString(fmt.Sprintf(" if c.%s == nil {\nreturn %s\n}\n", fieldName, defaultName))
	builder.WriteString(fmt.Sprintf("\treturn *c.%s\n}\n", fieldName))

	// getter with default
	builder.WriteString(fmt.Sprintf("func (c *%s) Get%sWithDefault(d %s) %s {\n", structName, fieldName, field.TypeName, field.TypeName))
	builder.WriteString(fmt.Sprintf("\tif c.%s == nil {\n\t\treturn d\n\t}\n\treturn *c.%s\n}\n", fieldName, fieldName))

	return builder.String()
}

func genFieldGetterAndSetterWithPointerType(structName string, field *Field) string {
	fieldName := genFieldName(field.Name)
	builder := strings.Builder{}
	typeName := field.TypeName

	// setter
	builder.WriteString(fmt.Sprintf("func (c *%s) Set%s(value %s) {\n", structName, fieldName, typeName))
	builder.WriteString(fmt.Sprintf("\tc.%s = value\n}\n", fieldName))

	// getter
	defaultName := fmt.Sprintf("%s%sDefault", structName, fieldName)
	switch field.TrueType {
	case FieldTypeMap:
		builder.WriteString(fmt.Sprintf("var %s = make(%s, 0)\n", defaultName, field.TypeName))
	default:
		builder.WriteString(fmt.Sprintf("var %s %v\n", defaultName, field.TypeName))
	}
	builder.WriteString(fmt.Sprintf("func (c *%s) Get%s() %s {\n", structName, fieldName, field.TypeName))
	builder.WriteString(fmt.Sprintf(" if c.%s == nil {\nreturn %s\n}\n", fieldName, defaultName))
	builder.WriteString(fmt.Sprintf("\treturn c.%s\n}\n", fieldName))

	// getter with default
	builder.WriteString(fmt.Sprintf("func (c *%s) Get%sWithDefault(d %s) %s {\n", structName, fieldName, field.TypeName, field.TypeName))
	builder.WriteString(fmt.Sprintf("\tif c.%s == nil {\n\t\treturn d\n\t}\n\treturn c.%s\n}\n", fieldName, fieldName))

	return builder.String()
}
