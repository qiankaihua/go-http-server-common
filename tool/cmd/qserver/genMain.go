package main

import (
	"fmt"
	"os/exec"
	"strings"
)

const (
	genMainFilename = "main.go"
	genMainPackage  = "main"
)

func GenMain(dir string) error {
	if dir == "" {
		dir = "."
	}
	exist, err := fileExists(dir + "/" + genMainFilename)
	if err != nil {
		return fmt.Errorf("gen main failed, check dir[%s/%s] failed, err=%s", dir, genMainFilename, err)
	}
	if exist {
		return nil
	}
	err = createDirAndFileWhenNotExist(dir, genMainFilename)
	if err != nil {
		return fmt.Errorf("gen main failed, create dir[%s/%s] failed, err=%s", dir, genMainFilename, err)
	}
	err = writeContentToFile(dir, genMainFilename, genMainContent())
	if err != nil {
		return fmt.Errorf("write to file[%s/%s.go] failed, err=%s", dir, genMainFilename, err)
	}

	cmd := exec.Command("gofmt", "-w", fmt.Sprintf("%s/%s", dir, genMainFilename))
	if output, err := cmd.CombinedOutput(); err != nil {
		fmt.Printf("gen main succ, but format file[%s/%s] failed, output=%s, err=%s\n", dir, genMainFilename, string(output), err)
	}
	return nil
}

func genMainContent() string {
	nameSlice := strings.Split(moduleName, "/")
	return fmt.Sprintf(`package %s

	import (
		"gitlab.com/qiankaihua/go-http-server-common/http"
		"%s"
	)
	
	func main() {
		server, err := http.GetHttp("127.0.0.1", "8999", "%s", false, nil)
		if err != nil {
			panic(err)
		}
		router.Register(server, "")
		err = server.RunHttpServer()
		if err != nil {
			panic(err)
		}
	}
`, genMainPackage, routerImportPath, nameSlice[len(nameSlice)-1])
}
