package main

import (
	"fmt"
	"os/exec"
	"strings"
)

//  api prefix
// router
// middlewire 单独文件，判断文件是否存在，指定的函数名是否存在，不存在的话新增，存在的话不作处理

const (
	genRouterDir            = "/router"
	genRouterPackage        = "router"
	genRouterFile           = "router_gen.go"
	genRouterMiddleWareFile = "middleware.go"
)

var (
	routerImportPath = "../router"
)

type urlNode struct {
	Name     string
	Url      string
	Method   map[HTTPMethod]*InterfaceDefine
	Children map[string]*urlNode
}

func GenRouter(dir string, data map[string]*InterfaceDefine) error {
	if moduleName != "" {
		routerImportPath = moduleName + genRouterDir
	}
	if dir == "" {
		dir = "."
	}
	if err := createDirWhenNotExist(dir + genRouterDir); err != nil {
		return fmt.Errorf("gen router failed, create dir[%s%s] failed, err=%s", dir, genRouterDir, err)
	}
	content, msList := genRouterContent(data)
	if err := createDirAndFileWhenNotExist(dir+genRouterDir, genRouterFile); err != nil {
		return fmt.Errorf("gen router failed, create router file[%s%s/%s] failed, err=%s", dir, genRouterDir, genRouterFile, err)
	}
	if err := writeContentToFile(dir+genRouterDir, genRouterFile, content); err != nil {
		return fmt.Errorf("gen router failed, write content to file[%s%s/%s] failed, err=%s", dir, genRouterDir, genRouterFile, err)
	}

	if ok, err := fileExists(dir + genRouterDir + "/" + genRouterMiddleWareFile); err != nil || !ok {
		err := createDirAndFileWhenNotExist(dir+genRouterDir, genRouterMiddleWareFile)
		if err != nil {
			return fmt.Errorf("gen router failed, create router middleware file[%s%s/%s] failed, err=%s", dir, genRouterDir, genRouterMiddleWareFile, err)
		}
		err = writeContentToFile(dir+genRouterDir, genRouterMiddleWareFile, genMiddleWareContent(msList))
		if err != nil {
			return fmt.Errorf("gen router failed, write content to file[%s%s/%s] failed, err=%s", dir, genRouterDir, genRouterMiddleWareFile, err)
		}
	} else {
		oldContent, err := getContentFromFile(dir+genRouterDir, genRouterMiddleWareFile)
		if err != nil {
			return fmt.Errorf("gen router failed, get old content from file[%s%s/%s] failed, err=%s", dir, genRouterDir, genRouterMiddleWareFile, err)
		}
		err = appendContentToFile(dir+genRouterDir, genRouterMiddleWareFile, genAppendMiddleWareContent(msList, oldContent))
		if err != nil {
			return fmt.Errorf("gen router failed, append content to file[%s%s/%s] failed, err=%s", dir, genRouterDir, genRouterMiddleWareFile, err)
		}
	}
	cmd := exec.Command("gofmt", "-s", "-w", dir+genRouterDir)
	if output, err := cmd.CombinedOutput(); err != nil {
		fmt.Printf("gen router succ, but format dir[%s] failed, output=%s, err=%s\n", dir+genRouterDir, string(output), err)
	}
	return nil
}

func getUrlSplitDefine(data map[string]*InterfaceDefine) map[string]*urlNode {
	res := &urlNode{
		Name:     "",
		Url:      "",
		Method:   map[HTTPMethod]*InterfaceDefine{},
		Children: map[string]*urlNode{},
	}
	for _, iDefine := range data {
		if len(iDefine.Url) <= 0 || iDefine.Url == "/" {
			if d, ok := res.Children["/"]; ok {
				d.Method[iDefine.HttpMethod] = iDefine
			}
			res.Children["/"] = &urlNode{
				Name: "_",
				Url:  "/",
				Method: map[HTTPMethod]*InterfaceDefine{
					iDefine.HttpMethod: iDefine,
				},
				Children: map[string]*urlNode{},
			}
		}
		targetUrl := iDefine.Url
		if iDefine.Url[0] == '/' {
			targetUrl = iDefine.Url[1:]
		}
		sp := strings.Split(targetUrl, "/")

		now := res
		for _, u := range sp {
			ur := "/" + u
			if nxt, ok := now.Children[ur]; ok {
				now = nxt
				continue
			} else {
				now.Children[ur] = &urlNode{
					Name:     now.Name + "_" + u,
					Url:      ur,
					Method:   map[HTTPMethod]*InterfaceDefine{},
					Children: map[string]*urlNode{},
				}
			}
			now = now.Children[ur]
		}
		now.Method[iDefine.HttpMethod] = iDefine
	}
	return res.Children
}

func genRouterSubContent(node *urlNode, lastParams string, level int, builder *strings.Builder) []string {
	res := make([]string, 0)

	builder.WriteString(strings.Repeat("\t", level) + "{\n")
	for k, v := range node.Method {
		builder.WriteString(strings.Repeat("\t", level))
		builder.WriteString(fmt.Sprintf("\t%s.%s(\"%s\", append(%s_%s_mws(), http.WarpHandler(handler.%s))...)\n", lastParams, k, node.Url, node.Name, strings.ToLower(string(v.HttpMethod)), v.Name))
		res = append(res, fmt.Sprintf("%s_%s_mws", node.Name, strings.ToLower(string(v.HttpMethod))))
	}

	if len(node.Children) > 0 {
		builder.WriteString(strings.Repeat("\t", level))
		builder.WriteString(fmt.Sprintf("\t%s := %s.Group(\"%s\", %s_mws()...)\n", node.Name, lastParams, node.Url, node.Name))
		res = append(res, fmt.Sprintf("%s_mws", node.Name))
	}

	for _, c := range node.Children {
		res = append(res, genRouterSubContent(c, node.Name, level+1, builder)...)
	}
	builder.WriteString(strings.Repeat("\t", level) + "}\n")

	return res
}

func genRouterContent(data map[string]*InterfaceDefine) (string, []string) {
	node := getUrlSplitDefine(data)
	res := make([]string, 0)

	builder := strings.Builder{}
	builder.WriteString("// auto generate router, do not edit it.\n\n")
	builder.WriteString(fmt.Sprintf("package %s\n", genRouterPackage))
	builder.WriteString("import (\n")
	builder.WriteString("\t\"gitlab.com/qiankaihua/go-http-server-common/http\"\n")
	builder.WriteString(fmt.Sprintf("\t\"%s\"\n", handlerImportPath))
	builder.WriteString(")\n\n")
	builder.WriteString("func Register(h *http.Http, apiPrefix string) {\n")
	builder.WriteString("\th.SetGlobalMiddleWare(globalMiddleWare()...)\n")
	builder.WriteString(fmt.Sprintf("\tapi := h.Group(apiPrefix + \"%s\")\n", apiPrefix))

	for _, n := range node {
		res = append(res, genRouterSubContent(n, "api", 1, &builder)...)
	}

	builder.WriteString("}\n\n")

	return builder.String(), res
}
func genMiddleWareContent(res []string) string {
	builder := strings.Builder{}
	builder.WriteString("// globalMiddleWare return global mw\n\n")
	builder.WriteString(fmt.Sprintf("package %s\n", genRouterPackage))
	builder.WriteString("import (\n")
	builder.WriteString("\t\"github.com/gin-gonic/gin\"\n")
	builder.WriteString("\t\"gitlab.com/qiankaihua/go-http-server-common/http\"\n")
	builder.WriteString(")\n\n")
	builder.WriteString("func globalMiddleWare() []gin.HandlerFunc {\n")
	builder.WriteString("\treturn http.DefaultMiddleWare()\n")
	builder.WriteString("}\n")
	for _, v := range res {
		builder.WriteString(fmt.Sprintf("func %s() []gin.HandlerFunc {\n", v))
		builder.WriteString("\treturn []gin.HandlerFunc{}\n")
		builder.WriteString("}\n")
	}
	return builder.String()
}
func genAppendMiddleWareContent(res []string, fileContent string) string {
	builder := strings.Builder{}
	for _, v := range res {
		if strings.Contains(fileContent, fmt.Sprintf("func %s() []gin.HandlerFunc {\n", v)) {
			continue
		}
		builder.WriteString(fmt.Sprintf("func %s() []gin.HandlerFunc {\n", v))
		builder.WriteString("\treturn []gin.HandlerFunc{}\n")
		builder.WriteString("}\n")
	}
	return builder.String()
}
