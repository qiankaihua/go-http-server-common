package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"sync/atomic"
)

var debugMode = int32(0)

func printDebugInfo(format string, args ...interface{}) {
	if atomic.LoadInt32(&debugMode) == 1 {
		fmt.Printf(format+"\n", args...)
	}
}

func fileExists(filename string) (bool, error) {
	s, err := os.Stat(filename)
	if err != nil && !os.IsNotExist(err) {
		return false, err
	}
	if err != nil {
		return false, nil
	}
	if s.IsDir() {
		return false, fmt.Errorf("filename exist, but is dir")
	}
	return true, nil
}

func createDirWhenNotExist(dir string) error {
	_, err := os.Stat(dir)
	// dir not exist
	if err != nil {
		err = os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			return fmt.Errorf("create dir[%s] failed, err=%s", dir, err)
		}
	}
	return nil
}

func createDirAndFileWhenNotExist(dir, filename string) error {
	_, err := os.Stat(dir)
	// dir not exist
	if err != nil {
		err = os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			return fmt.Errorf("create dir[%s] failed, err=%s", dir, err)
		}
	}
	file := dir + "/" + filename
	s, err := os.Stat(file)
	if err != nil {
		_, err = os.Create(file)
		if err != nil {
			return fmt.Errorf("filename[%s] create failed, err=%s", file, err)
		}
	} else if s.IsDir() {
		return fmt.Errorf("filename[%s] is exist, but is dir,", file)
	}
	return nil
}

func writeContentToFile(dir, filename, content string) error {
	f, err := os.OpenFile(dir+"/"+filename, os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return fmt.Errorf("open file[%s/%s] failed, err=%s", dir, filename, err)
	}
	defer f.Close()
	f.WriteString(content)
	return nil
}
func appendContentToFile(dir, filename, content string) error {
	f, err := os.OpenFile(dir+"/"+filename, os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		return fmt.Errorf("open file[%s/%s] failed, err=%s", dir, filename, err)
	}
	defer f.Close()
	f.WriteString(content)
	return nil
}
func getContentFromFile(dir, filename string) (string, error) {
	bytes, err := ioutil.ReadFile(dir + "/" + filename)
	if err != nil {
		return "", fmt.Errorf("read file[%s/%s] failed, err=%s", dir, filename, err)
	}
	return string(bytes), nil
}
