package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"sync/atomic"
)

const version = "v1.1.11"

func main() {
	// 输入 server name， toml path , dirname
	basePath := flag.String("dir", "", "target generate dir name, default = './'")
	filename := flag.String("toml", "", "define interface toml filename")
	debugFlag := flag.Bool("debug", false, "open debug mode to print more info")
	printVersion := flag.Bool("version", false, "print version")
	printVersionShort := flag.Bool("v", false, "print version")
	flag.Parse()

	if (printVersion != nil && (*printVersion)) || (printVersionShort != nil && (*printVersionShort)) {
		fmt.Println("version:", version)
		return
	}

	if *basePath == "" {
		*basePath = "./"
	}

	if debugFlag != nil && (*debugFlag) {
		atomic.StoreInt32(&debugMode, 1)
	}

	// parseFile
	interfaceMap, structMap, err := Parse(*filename)
	j, _ := json.Marshal(structMap)
	printDebugInfo("%s", string(j))
	if err != nil {
		fmt.Printf("[Error]parse [%s] conf failed, err = %v\n", *filename, err)
		os.Exit(-1)
	}

	// checker
	err = CheckStruct(structMap)
	if err != nil {
		fmt.Println("[Error]check struct define failed, err =", err)
		os.Exit(-1)
	}
	err = CheckInterface(interfaceMap, structMap)
	if err != nil {
		fmt.Println("[Error]check struct define failed, err =", err)
		os.Exit(-1)
	}

	// gen model
	err = GenModel(*basePath, structMap)
	if err != nil {
		fmt.Println("[Error]gen model failed, err =", err)
		os.Exit(-1)
	}

	// gen handler
	err = GenHandler(*basePath, interfaceMap)
	if err != nil {
		fmt.Println("[Error]gen handler failed, err =", err)
		os.Exit(-1)
	}

	// gen router
	err = GenRouter(*basePath, interfaceMap)
	if err != nil {
		fmt.Println("[Error]gen router failed, err =", err)
		os.Exit(-1)
	}

	// gen main
	err = GenMain(*basePath)
	if err != nil {
		fmt.Println("[Error]gen main.go failed, err =", err)
		os.Exit(-1)
	}

	if moduleName != "" {
		cmd := exec.Command("go", "mod", "init", moduleName)
		cmd.Dir = *basePath
		if exist, err := fileExists(*basePath + "/go.mod"); !exist || err != nil {
			if err != nil {
				fmt.Printf("file exist check error: err=%v\n", err)
			}
			if output, err := cmd.CombinedOutput(); err != nil {
				fmt.Printf("go mod init failed, moduleName = %s, output=%v, err=%v\n", moduleName, string(output), err)
				return
			}
		}
		cmd = exec.Command("go", "mod", "tidy")
		cmd.Dir = *basePath
		if err := cmd.Run(); err != nil {
			fmt.Printf("go mod tidy failed, err=%v\n", err)
			return
		}
		cmd = exec.Command("go", "mod", "download")
		cmd.Dir = *basePath
		if err := cmd.Run(); err != nil {
			fmt.Printf("go mod download failed, err=%v", err)
			return
		}
	}
}
