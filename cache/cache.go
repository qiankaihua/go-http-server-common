package cache

import (
	"fmt"
	"time"

	"github.com/dgraph-io/ristretto"
	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/marshaler"
	"github.com/eko/gocache/store"
)

type CacheDataType int

const (
	CommentType CacheDataType = 1
)

type CacheManager interface {
	Load(key interface{}) (interface{}, error)
	Set(key interface{}, data interface{}, cost int64) error
	SetWithTTL(key interface{}, data interface{}, cost int64, TTL time.Duration) error
	Delete(key interface{}) error
}

type cacheManagerImpl struct {
	cache     *cache.Cache
	marshaler *marshaler.Marshaler
}

type CacheOptions struct {
	ItemCountersNum    int64 // ristretto 计数器大小
	MaxCost            int64 // 最大的
	BufferItems        int64 // ristretto buffer 大小, 一般 64, 不设置默认 64
	Metrics            bool  // 是否打点
	IgnoreInternalCost bool  // ristretto 是否设置
}

func NewCacheManagerImpl(opts ristretto.Config) (CacheManager, error) {
	if opts.BufferItems == 0 {
		opts.BufferItems = 64
	}
	ristrettoCache, err := ristretto.NewCache(&opts)
	if err != nil {
		return nil, err
	}
	if ristrettoCache == nil {
		return nil, fmt.Errorf("new ristretto cache fail, got nil")
	}
	storeImpl := store.NewRistretto(ristrettoCache, &store.Options{
		Cost:       1000,               // 如果没有设置, 默认 cost 1000
		Expiration: time.Hour * 24 * 7, // 默认过期时间 7 天
	})
	if storeImpl == nil {
		return nil, fmt.Errorf("new go cache store fail, got nil")
	}
	cacheImpl := cache.New(storeImpl)
	if cacheImpl == nil {
		return nil, fmt.Errorf("new go cache fail, got nil")
	}
	marshalerImpl := marshaler.New(cacheImpl)
	if marshalerImpl == nil {
		return nil, fmt.Errorf("new marshaler fail, got nil")
	}
	return &cacheManagerImpl{
		cache:     cacheImpl,
		marshaler: marshalerImpl,
	}, nil
}

func (c *cacheManagerImpl) Load(key interface{}) (interface{}, error) {
	return c.cache.Get(key)
}

func (c *cacheManagerImpl) Set(key interface{}, data interface{}, cost int64) error {
	return c.SetWithTTL(key, data, cost, 0)
}
func (c *cacheManagerImpl) SetWithTTL(key interface{}, data interface{}, cost int64, TTL time.Duration) error {
	return c.cache.Set(key, data, &store.Options{
		Cost:       cost,
		Expiration: TTL,
	})
}

// func (c *cacheManagerImpl) SetWithResultStruct(key interface{}) error {
// 	return c.marshaler.Set()
// }
func (c *cacheManagerImpl) Delete(key interface{}) error {
	return c.cache.Delete(key)
}
