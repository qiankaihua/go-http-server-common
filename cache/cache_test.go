package cache

import (
	"testing"
	"time"

	"github.com/dgraph-io/ristretto"
	"github.com/stretchr/testify/require"
)

func TestCache(t *testing.T) {
	a := require.New(t)
	c, err := NewCacheManagerImpl(ristretto.Config{
		NumCounters:        0,
		MaxCost:            0,
		BufferItems:        0,
		Metrics:            false,
		IgnoreInternalCost: false,
	})
	a.NotNil(err)
	a.Nil(c)
	c, err = NewCacheManagerImpl(ristretto.Config{
		NumCounters:        10,
		MaxCost:            100,
		BufferItems:        1,
		Metrics:            false,
		IgnoreInternalCost: true,
	})
	a.NotNil(c)
	a.Nil(err)
	err = c.Set(1, "123", 10)
	a.Nil(err)
	err = c.Set(2, "123", 1000)
	a.Nil(err)
	err = c.SetWithTTL(3, "123", 10, 1)
	a.Nil(err)
	err = c.Set(4, "123", 10)
	a.Nil(err)
	err = c.Delete(4)
	a.Nil(err)
	time.Sleep(time.Microsecond * 100)
	res, err := c.Load(1)
	a.Nil(err)
	a.Equal("123", res)
	_, err = c.Load(2)
	a.NotNil(err)
	_, err = c.Load(3)
	a.NotNil(err)
	_, err = c.Load(4)
	a.NotNil(err)
}
