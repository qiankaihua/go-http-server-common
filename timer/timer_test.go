package timer

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTimer(t *testing.T) {
	a := assert.New(t)
	timer := NewTimer()
	timer.Start("test")
	timer.Start("test")
	time.Sleep(time.Microsecond)
	s := timer.Stop("test")
	a.True(s.Nanoseconds() > time.Microsecond.Nanoseconds())
	s2 := timer.Stop("test")
	a.True(s2 == s)
	d := timer.GetDuration("test")
	a.True(d.Nanoseconds() > time.Microsecond.Nanoseconds())
	d2 := timer.GetDuration("test")
	a.True(d == d2)
}

func TestTimerWithoutStop(t *testing.T) {
	a := assert.New(t)
	timer := NewTimer()
	timer.Start("test")
	timer.Start("test")
	time.Sleep(time.Microsecond)
	d := timer.GetDuration("test")
	a.True(d.Nanoseconds() > time.Microsecond.Nanoseconds())
	m := timer.GetUs("test")
	a.True(m > time.Microsecond.Microseconds())
}

func TestNoNameError(t *testing.T) {
	a := assert.New(t)
	timer := NewTimer()
	timer.Start("aaa")
	s := timer.Stop("test")
	a.Equal(s, ErrDuration)
	d := timer.GetDuration("test2")
	a.Equal(d, ErrDuration)
}
