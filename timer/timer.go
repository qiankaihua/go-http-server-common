package timer

import (
	"sync"
	"time"
)

var (
	ErrDuration = time.Duration(-1 * time.Second)
)

type Timer struct {
	timers map[string]time.Time
	record map[string]time.Duration
	lock   sync.Mutex
}

// 并发安全
func NewTimer() *Timer {
	return &Timer{
		timers: map[string]time.Time{},
		record: map[string]time.Duration{},
		lock:   sync.Mutex{},
	}
}

func (t *Timer) Start(name string) {
	t.lock.Lock()
	defer t.lock.Unlock()
	_, ok := t.timers[name]
	if ok {
		return
	}
	t.timers[name] = time.Now()
}

func (t *Timer) Stop(name string) time.Duration {
	t.lock.Lock()
	defer t.lock.Unlock()
	if d, ok := t.record[name]; ok {
		return d
	}
	if s, ok := t.timers[name]; ok {
		d := time.Since(s)
		t.record[name] = d
		return d
	}
	return ErrDuration
}

func (t *Timer) GetDuration(name string) time.Duration {
	t.lock.Lock()
	defer t.lock.Unlock()
	if d, ok := t.record[name]; ok {
		return d
	}
	if s, ok := t.timers[name]; ok {
		return time.Since(s)
	}
	return ErrDuration
}

func (t *Timer) GetUs(name string) int64 {
	return t.GetDuration(name).Nanoseconds() / 1000
}
