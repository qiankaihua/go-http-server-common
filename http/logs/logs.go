package logs

import (
	"fmt"

	"gitlab.com/qiankaihua/go-http-server-common/http"
)

func Error(message string) {
	ErrorF(message)
}

func ErrorF(format string, args ...any) {
	if logger := http.GetHttpLogger(); logger != nil {
		logger.ErrorF(format, args...)
	} else {
		fmt.Printf("[ERROR]"+format+"\n", args...)
	}
}

func Warn(message string) {
	WarnF(message)
}

func WarnF(format string, args ...any) {
	if logger := http.GetHttpLogger(); logger != nil {
		logger.WarnF(format, args...)
	} else {
		fmt.Printf("[WARN]"+format+"\n", args...)
	}
}

func Info(message string) {
	InfoF(message)
}

func InfoF(format string, args ...any) {
	if logger := http.GetHttpLogger(); logger != nil {
		logger.InfoF(format, args...)
	} else {
		fmt.Printf("[INFO]"+format+"\n", args...)
	}
}
