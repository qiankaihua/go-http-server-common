package http

import (
	"io/ioutil"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
)

func getTestRequest(uri string, router *gin.Engine, origin string) (*httptest.ResponseRecorder, []byte) {
	// 构造get请求
	req := httptest.NewRequest("GET", uri, nil)
	if origin != "" {
		req.Header.Add("origin", origin)
	}
	// 初始化响应
	w := httptest.NewRecorder()

	// 调用相应的handler接口
	router.ServeHTTP(w, req)

	// 提取响应
	result := w.Result()
	defer result.Body.Close()

	// 读取响应body
	body, _ := ioutil.ReadAll(result.Body)
	return w, body
}

func TestRecoverMiddleware(t *testing.T) {
	engine := gin.Default()
	engine.GET("/test", WarpHandler(RecoverMiddleWare), func(c *gin.Context) {
		panic(123)
	})
	getTestRequest("/test", engine, "")
}

func TestAccessMiddleware(t *testing.T) {
	engine := gin.Default()
	engine.GET("/test", WarpHandler(AccessMiddleWare), func(c *gin.Context) {

	})
	getTestRequest("/test", engine, "")
}

func TestCorsMiddleware(t *testing.T) {
	a := require.New(t)
	engine := gin.Default()
	engine.GET("/test", CorsMiddleWare(), func(c *gin.Context) {

	})
	resp, _ := getTestRequest("/test", engine, "")
	a.Equal("*", resp.Header().Get("Access-Control-Expose-Headers"))
	resp, _ = getTestRequest("/test", engine, "test")
	a.Equal("*", resp.Header().Get("Access-Control-Expose-Headers"))
}
