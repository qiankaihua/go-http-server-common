package http

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/qiankaihua/go-http-server-common/log"
	"gitlab.com/qiankaihua/go-http-server-common/timer"
)

var (
	defaultMiddleWare             = []gin.HandlerFunc{WarpHandler(RecoverMiddleWare), WarpHandler(AccessMiddleWare)}
	loggerImpl        *log.Logger = nil
	httpImpl          *Http       = nil
	locker            *sync.Mutex = &sync.Mutex{}
)

type HandlerFunc func(*Context)

func WarpHandler(handler HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		context := &Context{
			Context:     c,
			Timer:       timer.NewTimer(),
			Extra:       sync.Map{},
			NoticeEntry: log.NewNoticeEntry(),
			logid:       "",
		}
		handler(context)
	}
}

type SignalHandler func(shutdownError error)
type Http struct {
	server              *gin.Engine
	port                string
	addr                string
	sigMap              map[os.Signal][]SignalHandler
	gracefulQuitTimeout time.Duration
}

type HttpOptions struct {
	GracefulQuitTimeout time.Duration
}

func GetHttp(addr, port, serverName string, debugMode bool, logger *log.Logger) (*Http, error) {
	return GetHttpWithOptions(addr, port, serverName, debugMode, logger, HttpOptions{
		GracefulQuitTimeout: time.Second * 30,
	})
}

func GetHttpWithOptions(addr, port, serverName string, debugMode bool, logger *log.Logger, opts HttpOptions) (*Http, error) {
	locker.Lock()
	defer locker.Unlock()
	if httpImpl != nil {
		return httpImpl, nil
	}
	loggerImpl = logger
	if loggerImpl == nil {
		loggerImpl = log.DefaultLogger
		if loggerImpl.LogEngine == nil {
			defaultLogLevel := log.LogLevelInfo
			if debugMode {
				defaultLogLevel = log.LogLevelDebug
			}
			l, err := log.NewLogger(log.LoggerConf{
				ServerName:      serverName,
				FilterFunc:      nil,
				LogMinLevel:     defaultLogLevel,
				LogDirName:      "./log",
				NoticeFileLevel: log.DefaultNoticeFileLevel,
				WfFileLevel:     log.DefaultNoticeWfFileLevel,
			})
			if err != nil {
				return nil, fmt.Errorf("init default logger failed, err=%v", err)
			}
			loggerImpl = l
		}
	}
	if opts.GracefulQuitTimeout == 0 {
		opts.GracefulQuitTimeout = time.Second * 30
	}
	httpImpl = &Http{
		server:              gin.Default(),
		port:                port,
		addr:                addr,
		sigMap:              map[os.Signal][]SignalHandler{},
		gracefulQuitTimeout: opts.GracefulQuitTimeout,
	}
	return httpImpl, nil
}

func (h *Http) Group(relativePath string, handlers ...gin.HandlerFunc) *gin.RouterGroup {
	return h.server.Group(relativePath, handlers...)
}

func (h *Http) RunHttpServer() error {
	return h.server.Run(h.addr + ":" + h.port)
}

func (h *Http) RunHttpServerWithGracefulQuit() {
	server := http.Server{
		Addr:    h.addr + ":" + h.port,
		Handler: h.server.Handler(),
	}
	// 启动 server
	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			h.GetLogger().FatalF("err=%v", err)
			panic(err)
		}
	}()
	// 注册信号
	sigChan := make(chan os.Signal, 1)
	sigs := make([]os.Signal, 0, len(h.sigMap))
	for s := range h.sigMap {
		sigs = append(sigs, s)
	}
	sigs = append(sigs, syscall.SIGINT, syscall.SIGTERM)
	signal.Notify(sigChan, sigs...)

	for {
		sig := <-sigChan
		handlers := h.sigMap[sig]
		h.GetLogger().InfoF("receive signal %s", sig.String())
		var err error
		if sig == syscall.SIGINT || sig == syscall.SIGTERM {
			ctx, cannel := context.WithTimeout(context.Background(), h.gracefulQuitTimeout)
			defer cannel()
			err = server.Shutdown(ctx)
			if err != nil {
				h.GetLogger().FatalF("shutdown err=%v", err)
			}
		}
		for _, h := range handlers {
			h(err)
		}
		if sig == syscall.SIGINT || sig == syscall.SIGTERM {
			break
		}
	}
	h.GetLogger().Info("exist finish...")
}

func DefaultMiddleWare() []gin.HandlerFunc {
	return defaultMiddleWare
}

func (h *Http) SetGlobalMiddleWare(mws ...gin.HandlerFunc) gin.IRoutes {
	return h.server.Use(mws...)
}

func (h *Http) GetLogger() *log.Logger {
	return loggerImpl
}

// 并发不安全
func (h *Http) AppendSignalHandle(sig []os.Signal, handle []SignalHandler) {
	for _, s := range sig {
		h.sigMap[s] = append(h.sigMap[s], handle...)
	}
}

func GetHttpLogger() *log.Logger {
	return loggerImpl
}
