package http

import (
	"runtime/debug"
)

func RecoverMiddleWare(c *Context) {
	defer func() {
		if err := recover(); err != nil {
			c.ErrorF("Panic[%v] Stack: %s", err, string(debug.Stack()))
		}
	}()
	c.Next()
}
