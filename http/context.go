package http

import (
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/hashicorp/go-uuid"
	"gitlab.com/qiankaihua/go-http-server-common/log"
	"gitlab.com/qiankaihua/go-http-server-common/timer"
)

// context 封装了 gin.Context, 但是新增的字段/方法不保证并发安全
type Context struct {
	*gin.Context
	*log.NoticeEntry

	Timer *timer.Timer
	Extra sync.Map

	logid string
}

const (
	LOG_ID_HEADER = "log-id"
)

func (c *Context) GetLogId() string {
	if c.logid != "" {
		return c.logid
	}
	logid := c.GetHeader(LOG_ID_HEADER)
	if logid == "" {
		if l, err := uuid.GenerateUUID(); err == nil {
			logid = l
		}
	}
	c.logid = logid
	return logid
}

func (c *Context) PrintAccessNotice() {
	loggerImpl.InfoF("[log_id=%s]%s", c.GetLogId(), c.NoticeEntry.String())
}

func (c *Context) getLogidLog() string {
	return "[log_id=" + c.GetLogId() + "]"
}

func (c *Context) Debug(params ...interface{}) {
	loggerImpl.Debug(append([]interface{}{c.getLogidLog()}, params...)...)
}
func (c *Context) DebugF(format string, params ...interface{}) {
	loggerImpl.DebugF("[log_id=%s]"+format, append([]interface{}{c.GetLogId()}, params...)...)
}
func (c *Context) Info(params ...interface{}) {
	loggerImpl.Info(append([]interface{}{c.getLogidLog()}, params...)...)
}
func (c *Context) InfoF(format string, params ...interface{}) {
	loggerImpl.InfoF("[log_id=%s]"+format, append([]interface{}{c.GetLogId()}, params...)...)
}
func (c *Context) Warn(params ...interface{}) {
	loggerImpl.Warn(append([]interface{}{c.getLogidLog()}, params...)...)
}
func (c *Context) WarnF(format string, params ...interface{}) {
	loggerImpl.WarnF("[log_id=%s]"+format, append([]interface{}{c.GetLogId()}, params...)...)
}
func (c *Context) Error(params ...interface{}) {
	loggerImpl.Error(append([]interface{}{c.getLogidLog()}, params...)...)
}
func (c *Context) ErrorF(format string, params ...interface{}) {
	loggerImpl.ErrorF("[log_id=%s]"+format, append([]interface{}{c.GetLogId()}, params...)...)
}
func (c *Context) Fatal(params ...interface{}) {
	loggerImpl.Fatal(append([]interface{}{c.getLogidLog()}, params...)...)
}
func (c *Context) FatalF(format string, params ...interface{}) {
	loggerImpl.FatalF("[log_id=%s]"+format, append([]interface{}{c.GetLogId()}, params...)...)
}
func (c *Context) Panic(params ...interface{}) {
	loggerImpl.Panic(append([]interface{}{c.getLogidLog()}, params...)...)
}
func (c *Context) PanicF(format string, params ...interface{}) {
	loggerImpl.PanicF("[log_id=%s]"+format, append([]interface{}{c.GetLogId()}, params...)...)
}
