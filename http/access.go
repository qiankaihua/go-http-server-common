package http

import (
	"fmt"
	"net/url"
)

func AccessMiddleWare(c *Context) {
	method := "-"
	u := "-"
	callFrom := "-"
	status := "-"
	var err error
	if c.Request != nil {
		method = c.Request.Method
		u, err = url.QueryUnescape(c.Request.URL.String())
		if err != nil {
			c.WarnF("URL decode error: raw=%s, err=%s", c.Request.URL.String(), err)
			u = c.Request.URL.String()
		}
		callFrom = c.ClientIP()
	}
	c.PushNotice("method", method)
	c.PushNotice("url", u)
	c.PushNotice("remote_ip", callFrom)
	c.Timer.Start("all_time")
	c.Next()
	d := c.Timer.Stop("all_time")
	c.PushNotice("access_time", d)
	// notice.PushNotice("call_from", c.Request.RemoteAddr)

	if c.Writer != nil {
		status = fmt.Sprintf("%3d", c.Writer.Status())
	}
	c.PushNotice("status", status)
	c.PrintAccessNotice()
}
