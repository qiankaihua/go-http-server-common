package http

import (
	h "net/http"
	"net/textproto"
	"regexp"
	"strings"
	"sync"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/qiankaihua/go-http-server-common/log"
	"gitlab.com/qiankaihua/go-http-server-common/mock"
	"gitlab.com/qiankaihua/go-http-server-common/timer"
)

func TestContextGetLogid(t *testing.T) {
	a := initForHttpTest(t)
	context := &Context{
		Context: &gin.Context{
			Request: &h.Request{
				Header: map[string][]string{
					textproto.CanonicalMIMEHeaderKey(LOG_ID_HEADER): {"test"},
				},
			},
			Writer:   nil,
			Params:   []gin.Param{},
			Keys:     map[string]interface{}{},
			Errors:   []*gin.Error{},
			Accepted: []string{},
		},
		NoticeEntry: log.NewNoticeEntry(),
		Timer:       timer.NewTimer(),
		Extra:       sync.Map{},
	}
	a.Equal("test", context.GetLogId())
	contextWithoutLogid := &Context{
		Context: &gin.Context{
			Request: &h.Request{
				Header: map[string][]string{},
			},
		},
	}
	l := contextWithoutLogid.GetLogId()
	a.Regexp(regexp.MustCompile(`^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$`), l)
	a.Equal(l, contextWithoutLogid.GetLogId())
	a.Equal(l, contextWithoutLogid.logid)
}

func TestContextLogger(t *testing.T) {
	a := initForHttpTest(t)

	engine := &mock.LogEngine{
		Last: map[mock.LogLevel]string{},
	}
	logger, _ := log.NewLogger(log.LoggerConf{
		ServerName:      "test",
		LogMinLevel:     "Debug",
		LogDirName:      "output/log",
		NoticeFileLevel: log.DefaultNoticeFileLevel,
		WfFileLevel:     log.DefaultNoticeWfFileLevel,
	})
	logger.LogEngine["notice"] = engine
	logger.LogEngine["wf"] = engine

	_, err := GetHttp("", "", "", true, logger)
	a.Nil(err)
	c := &Context{
		Context: &gin.Context{
			Request: &h.Request{
				Header: map[string][]string{},
			},
		},
		NoticeEntry: &log.NoticeEntry{},
		Timer:       &timer.Timer{},
		Extra:       sync.Map{},
		logid:       "",
	}
	c.PushNotice("test", 123)
	c.PrintAccessNotice()
	a.Contains(engine.Last[mock.LogLevelInfo], "test")
	a.Contains(engine.Last[mock.LogLevelInfo], "123")

	a.Equal("[log_id", strings.Split(c.getLogidLog(), "=")[0])

	c.Debug("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelDebug, "", c.getLogidLog(), "123", 1231, "fds"))
	c.Info("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelInfo, "", c.getLogidLog(), "123", 1231, "fds"))
	c.Warn("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelWarning, "", c.getLogidLog(), "123", 1231, "fds"))
	c.Error("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelError, "", c.getLogidLog(), "123", 1231, "fds"))
	c.Fatal("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelFatal, "", c.getLogidLog(), "123", 1231, "fds"))
	c.Panic("123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelPanic, "", c.getLogidLog(), "123", 1231, "fds"))
	c.DebugF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelDebug, "%s%s, %d, %v", c.getLogidLog(), "123", 1231, "fds"))
	c.InfoF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelInfo, "%s%s, %d, %v", c.getLogidLog(), "123", 1231, "fds"))
	c.WarnF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelWarning, "%s%s, %d, %v", c.getLogidLog(), "123", 1231, "fds"))
	c.ErrorF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelError, "%s%s, %d, %v", c.getLogidLog(), "123", 1231, "fds"))
	c.FatalF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelFatal, "%s%s, %d, %v", c.getLogidLog(), "123", 1231, "fds"))
	c.PanicF("%s, %d, %v", "123", 1231, "fds")
	a.True(engine.Check(mock.LogLevelPanic, "%s%s, %d, %v", c.getLogidLog(), "123", 1231, "fds"))
}
