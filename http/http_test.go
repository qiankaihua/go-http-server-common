package http

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
)

func initForHttpTest(t *testing.T) *require.Assertions {
	httpImpl = nil
	loggerImpl = nil
	return require.New(t)
}

func TestHttp(t *testing.T) {
	a := initForHttpTest(t)

	addr := "a"
	port := "p"

	s, err := GetHttp(addr, port, "test", true, nil)

	a.Equal(addr, s.addr)
	a.Equal(port, s.port)
	a.Nil(err)
	a.Equal(defaultMiddleWare, DefaultMiddleWare())
	s.SetGlobalMiddleWare()
}

func TestWarpHandle(t *testing.T) {
	a := initForHttpTest(t)

	test := 1234
	target := 4321
	f := func(c *Context) {
		test = target
	}
	h := WarpHandler(f)
	h(&gin.Context{})
	a.Equal(test, target)
}
