package common

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPathExists(t *testing.T) {
	a := assert.New(t)

	ok, err := PathExists("/path/not/exist")
	a.Nil(err)
	a.False(ok)

	ok, err = PathExists("/tmp")
	a.Nil(err)
	a.True(ok)
}

func TestFileExists(t *testing.T) {
	a := assert.New(t)

	ok, err := FileExists("/path/not/exist")
	a.Nil(err)
	a.False(ok)

	ok, err = FileExists("/tmp")
	a.NotNil(err)
	a.False(ok)

	os.Create("/tmp/go_http_server_common_testing_file_exist")
	ok, err = FileExists("/tmp/go_http_server_common_testing_file_exist")
	a.Nil(err)
	a.True(ok)
}
