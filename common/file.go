package common

import (
	"fmt"
	"os"
)

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func FileExists(filename string) (bool, error) {
	s, err := os.Stat(filename)
	if err != nil && !os.IsNotExist(err) {
		return false, err
	}
	if err != nil {
		return false, nil
	}
	if s.IsDir() {
		return false, fmt.Errorf("filename exist, but is dir")
	}
	return true, nil
}
