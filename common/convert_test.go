package common

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type Sub struct {
	Test int64
}
type SearchReq struct {
	FromID     int64   `form:"fromID" json:"fromID" query:"fromID" binding:"required"`
	Limit      uint64  `form:"limit" json:"limit" query:"limit" binding:"required"`
	Query      string  `form:"query" json:"query" query:"query" binding:"required"`
	TestNil    *string `form:"test_nil" json:"test_nil" query:"test_nil"`
	TestStruct Sub     `form:"sub" json:"sub" query:"sub" binding:"sub"`
}

func TestXxx(t *testing.T) {
	a := require.New(t)
	mp, err := ConvertGetRequestToMap(SearchReq{
		FromID: 123,
		Limit:  123,
		Query:  "123",
	})
	a.Nil(err)
	a.Contains(mp, "fromID")
	a.Contains(mp, "limit")
	a.Contains(mp, "query")
	a.Contains(mp, "sub")
	a.Equal(mp["fromID"], "123")
	a.Equal(mp["limit"], "123")
	a.Equal(mp["query"], "123")
	a.Equal(mp["sub"], "{\"Test\":0}")
	mp, err = ConvertGetRequestToMap(&SearchReq{
		FromID: 123,
		Limit:  123,
		Query:  "123",
	})
	a.Nil(err)
	a.Contains(mp, "fromID")
	a.Contains(mp, "limit")
	a.Contains(mp, "query")
	a.Contains(mp, "sub")
	a.Equal(mp["fromID"], "123")
	a.Equal(mp["limit"], "123")
	a.Equal(mp["query"], "123")
	a.Equal(mp["sub"], "{\"Test\":0}")
	mp, err = ConvertGetRequestToMap(123)
	a.NotNil(err)
	a.Empty(mp)
}
