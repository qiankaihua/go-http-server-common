package common

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

func ConvertGetRequestToMap(req interface{}) (map[string]string, error) {
	res := map[string]string{}
	v := reflect.ValueOf(req)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	if v.Kind() != reflect.Struct { // 非结构体返回错误提示
		// fmt.Errorf("ToMap only accepts struct or struct pointer; got %T", v)
		return res, fmt.Errorf("only request struct can convert to Map, but got %T", v)
	}
	t := v.Type()
	segData := ""
	// 遍历结构体字段
	// 指定 query tag 值为 map 中 key
	for i := 0; i < v.NumField(); i++ {
		fi := t.Field(i)
		if tagValue := strings.Split(fi.Tag.Get("query"), ",")[0]; tagValue != "" {
			// 空指针不透传
			if v.Field(i).Kind() == reflect.Ptr && v.Field(i).IsNil() {
				continue
			}
			switch {
			case v.Field(i).CanInt():
				data := v.Field(i).Int()
				segData = strconv.FormatInt(data, 10)
			case v.Field(i).CanUint():
				data := v.Field(i).Uint()
				segData = strconv.FormatUint(data, 10)
			case v.Field(i).Kind() == reflect.String:
				segData = v.Field(i).String()
			default:
				v, _ := json.Marshal(v.Field(i).Interface())
				segData = string(v)
			}
			res[tagValue] = segData
		}
	}
	return res, nil
}
